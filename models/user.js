const {Schema, model} =require('mongoose');

const userSchema = new Schema({

    Nombre: String,
    Apellido: String,
    Email: String,
    Pass: String,
    Pais:String,
    Sexo: String,
    Edad: Number,
    Status: Boolean
});

module.exports = model("worshop_users", userSchema)