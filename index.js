const express = require ("express");
require("dotenv").config();
const app = express();
const cors = require("cors");
const {dbConnection} = require("./database/config")
const USERS = require("./models/user")

app.use(cors());

app.use(express.json())

dbConnection();


//----------------------------------------------------
//   Endpoint de tipo POST para dar de alta usuarios
//----------------------------------------------------
app.post("/api/get/users", async (req, res)=>{
    //console.log(req);
    

    const {Nombre, Apellido, Email, Pass, Pais, Sexo, Edad, Status}= req.body;

    console.log(`Llega al controlador Nombre:${Nombre}, Apellido: ${Apellido}, Email: ${Email}, Pass: ${Pass}, Pais: ${Pais}, Sexo: ${Sexo}, Edad: ${Edad}, Status: ${Status}`)

    const user = new USERS();
    user.Nombre= Nombre;
    user.Apellido=Apellido;
    user.Email=Email;
    user.Pass=Pass;
    user.Pais=Pais;
    user.Sexo=Sexo;
    user.Edad=Edad;
    user.Status=Status;

    await user.save();

    console.log("Modelo parseado del usuario", user);

    console.log("ejecutando ruta api post users")
    console.log("creando usuario");
    console.log("----------------------------------------------------");
    res.status(201).json({
        ok:true,
        msg:"Usuario creado correctamente",
        data: user
    })
});

//----------------------------------------------------
//   Endpoint tipo get que retorna todos los usuarios
//----------------------------------------------------
app.get("/api/get/users", (req, res)=>{
    console.log(req);
    console.log("ejecutando ruta api get all users")
    console.log("consultando todos los usuarios");
    console.log("----------------------------------------------------");
});

//----------------------------------------------------
//  Endoint Get User By Id - Retorna un usuario por Id
//----------------------------------------------------
app.get("/api/get/users/:idUser", (req, res)=>{
    const {idUser, name} = req.params;

    console.log(`Id usuario: ${idUser}`);
    console.log("ejecutando ruta api get user by id")
    console.log("consultando un usuario por id");
    console.log("----------------------------------------------------");
});

//----------------------------------------------------
//  Endpoint para actualizar un usuario
//----------------------------------------------------
app.put("api/get/users/:idUser", (req, res)=>{
    //console.log(req.params.idUsers);
    console.log("ejecutando ruta api put user")
    console.log("actualizando un usuario por id");
    console.log("----------------------------------------------------");
});

//---------------------------------------------------
// Endpoint para eliminar un usuario
//---------------------------------------------------

app.delete("api/get/users/:idUser", (req, res)=> {
    console.log(req);
    console.log("ejecutando ruta api delete user")
    console.log("eliminado un usuario por id");
    console.log("----------------------------------------------------");
});



app.listen(process.env.PORT, ()=>{
    console.log(`servidor esta corriendo en el puerto: ${process.env.PORT}`);

})

